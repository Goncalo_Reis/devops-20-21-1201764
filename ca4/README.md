# Class Assignment 2 Report

##Docker

The goal of this assignment is to use Docker to set up a containerized environment to
execute the gradle version of spring basic tutorial application.

First, we should go to www.docker.com/products/docker-desktop, download the docker installer and install it.

The, we should go to the provided repository
(https://bitbucket.org/atb/docker-compose-spring-tut-demo/) to study and explore the docker and
docker-compose files.

To set them up to our specific case, the first thing we have to do is change the repository
that the web container uses to our own, like this:

![git_clone](./media/git_clone.png)

Then, update the WORKDIR command so that it reflects your path to the tut-basic-gradle project. In my case,
that looks like this:

![workdir](./media/workdir.png)

Next, you should add a command to give the necessary permissions regarding the use of gradlew, as such:

![chmod](./media/chmod.png)

Finally, you should run ```docker-compose up``` to set up the VM's, and then, in your browser,
you can run http://localhost:8080/demo-0.0.1-SNAPSHOT/ to see the fronend, like this:

![frontend_success](./media/frontend_success.png)

and the h2-console, with the connection string jdbc:h2:tcp://192.168.33.11:9092/./jpadb:

![h2_success](./media/h2_success.png)

Then, we want to push images of these two containers to docker hub. To do it, we have to:

- Create an account at hub.docker.com;

- Use docker commit {container-name} {new-image-name} to create a new image from each container we are using.

- Use docker tag {image_id} {image_name}:{new_tag} to create a new tag to each of these images.

- Finally, use docker push {image_name}:{image_tag} to push it to remote.

Finally, we want to save the database file of the db container to the data folder of our machine. To do it:

- With the containers running, enter the db one by using docker-compose exec db /bin/bash.

- Use ls -l to see the name of the file you want to copy (see image below).

- Exit the container and use docker cp ca4_db_1:/usr/src/app/jpadb.mv.db .\data to copy the file from the container to your machine.

![db_copy](./media/db_copy.png)

## Alternative Analysis - Kubernetes

Docker is extremely useful as a provider of containers that are used to, basically, code once and run anywhere: Kubernetes provides
the potential to manage all your container resources from a single point. Kubernetes also has built-in isolation mechanism like
namespaces which allows us to group container resources by access permission, staging environments and more. Thus, combining DevOps
practices with containers and Kubernetes further enables a baseline of microservices architecture that promotes fast
delivery and scalable orchestration of cloud-native applications.

In summary, using kubernetes with docker can have the following advantages:

- Make your infrastructure more robust and your app more highly available. Your app will remain online, even if some of the nodes go offline.
- Make your application more scalable. If your app starts to get a lot more load and you need to scale out to be able to provide a better
user experience, it’s simple to spin up more containers or add more nodes to your Kubernetes cluster.
- Docker provides an open standard for packaging and distributing containerized applications.
- Using Docker, you can build and run containers, and store and share container images. One can easily run a Docker build on a

## Alternative Implementation

To use kubernetes as a solution in this alternative, we have to do some things before actually starting to use it:

- Install minikube. Go to https://minikube.sigs.k8s.io/docs/start/ to download and then install.

- Install kompose. Go to  https://kompose.io/ and download the exe.

- Then, move the exe file you downloaded to the folder next to your docker-composer file, and run ```kompose-windows-amd64.exe convert```.

- In the newly created files, change, in both db-development.yaml and web-development.yaml the following line to reflect
  the image you posted to docker hub and want to use, like this:

![deployment_changes](./media/deployment_changes.png)

- Next, add in both db-service.yaml and web-service.yaml the line ```type:NodePort```, as per below:

![services_changes](./media/services_changes.png)

- Finally, you have to change, in the default-networkpolicy.yaml the first line to apiVersion:networking.k8s.io/v1, such as below:

![default-networkpolicy_changes](./media/default-networkpolicy_changes.png)

After all this, we are ready to start the minikube. Remember to open your command line with admin permissions so that hyper-v
can be used, and type ```minikube --memory 4000 start -vm-driver hyperv```. The memory option allows us to increase the memory RAM
that we give the VM's, and the vm-driver option allows us to force the use of hyper-v as a provider.

Then, don't forget to do ```docker login```, because the VM's will be created with images you posted to docker hub.

Use ```kubectl apply -f.``` to set up the cluster and the pods within it.

Then, use ```kubectl get pod -o wide``` and take note of the IP addresses of both the DB and WEB VM's. If no IP's show up,
  wait a few moments and try again later: the process takes some time.

After that, use ```get pods``` and take note of the name of the web pod, we are going to use it in the next step.

Next, we must enter the Web VM. To do it, use ```kubectl exec --stdin --tty {web_name} -- /bin/bash``` and then
  use ```apt install nano``` to install the nano text editor, that will be needed in the next step.
  
Navigate until src/main/resources and then do ```nano application.properties``` and change the IP address present in the datasource.url
line to the db one you took note a few steps earlier.

Go back to the main folder of the project (still inside the VM, of course), and do ```./gradlew clean build``` to build the
project again, after this modification.

Then, use ```cp build/libs/demo-0.0.1 -SNAPSHOT.war /usr/local/tomcat/webapps/``` to copy the war file into tomcat. You have
to redo that step because we just did a clean build. 

- At last, use ```minikube service web``` to map the internal IP of the cluster to the web one, so it can contact the outside.

- Use http://{IP_of_WEB_VM}:30067/demo-0.0.1-SNAPSHOT/ to see the frontend of the application, and
  http://{IP_of_WEB_VM}:30067/demo-0.0.1-SNAPSHOT/h2-console/ with the connection string jdbc:h2:tcp://{IP_of_DB_VM}:9092/./jpadb, just as below:

![frontend_success_2](./media/frontend_success_2.png)

![h2_success_2](./media/h2_success_2.png)