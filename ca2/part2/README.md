# Class Assignment 2 part 2 Report

- git branch tut-basic-gradle

- git checkout tut-basic-gradle

- ./gradlew tasks - allows you to see what tasks are defined already.

- change the relevant src folders, so the tut-basic that was run in ca1 is also run here.

- ./gradlew bootRun - to test it (it should not display anything).

- add a necessary plugin according to instructions.

- ./gradlew build - to build the project

- ./gradlew bootRun - now it should run perfectly.

- git add . 
  
- git commit -m "tut-basic working with gradle"

- git push origin tut-basic-gradle

- add to build.gradle this code in order to add a task to copy the Jar file into a dist folder:
```  
task copyJar(type: Copy) {
  from "build/libs"
  into "dist"
  }
```

- git add .
  
- git commit -m "copy task added"
  
- git push origin tut-basic-gradle

- add to build.gradle this code in order to extend the clean task to also delete the built folder:
```
clean {
	delete "src/main/resources/static/built"
}
```

- git add .

- git commit -m "clean task updated"

- git push origin tut-basic-gradle

- git checkout master

- git merge tut-basic-gradle

- git tag -a ca2-part2 -m "end of assignment 2 part 2"

- git push origin ca2-part2

## Analysis of Alternative

Gradle is a build automation tool for multi-language software development.
It controls the development process in the tasks of compilation and packaging
to testing, deployment, and publishing.

Apache Ant is a software tool for automating software build processes which
originated from the Apache Tomcat project in early 2000 as a replacement for the Make build tool of Unix.
It is similar to Make, but is implemented using the Java language and requires the Java platform.
Unlike Make, which uses the Makefile format, Ant uses XML to describe the code build process and its dependencies.

Unfortunately, springboot does not support particularly well alternatives other than maven or gradle. They strongly
recommend users to choose a build system that supports dependency management and that can consume
artifacts published to the “Maven Central” repository, which Ant is not capable of. Because of this, the project was all
built with gradle and ant was used to generate the required tasks for this assignment.

There are some key differences between gradle and ant, namely:

- Gradle is groovy-based, while Ant is java-based;

- Gradle uses a domain specific language instead of XML, which Ant uses;

- Gradle was developed to overcome the shortcoming of both Maven and Ant, while Ant was developed to overcome the
  drawbacks of the Make build tool.
  
- Gradle plugins are coded in Java or Groovy, while Ant does not force any coding convention;

- Gradle provides a structured build, while Ant does not impose a structured project;

- Gradle provides seamless integration with IDE's, while in Ant the process is more complex;

- Gradle is more standardized than Ant in what regards flexibility;

- Gradle supports multi-project build, which Ant does not.


## Implementation of Alternative

- git branch tut-basic-ant

- git checkout tut-basic-ant

- ./gradlew tasks - allows you to see what tasks are defined already.

- make changes in src folder

- ./gradlew bootRun - to test it (it should not display anything).

- add a necessary plugin according to instructions.

- ./gradlew build - to build the project

- ./gradle2 bootRun - now it should run perfectly.

- git add .

- git commit -m "tut-basic working with ant"

- git push origin tut-basic-ant

- add to build.gradle this code in order to add a task to copy the Jar file into a dist folder:
```
task copyJar {ant.copy(file: 'build/libs/demo-0.0.1-SNAPSHOT.jar', todir: 'dist')}
```

- git add .

- git commit -m "copy task added"

- git push origin tut-basic-ant

- add to build.gradle this code in order to extend the clean task to also delete the built folder:
```
clean { doLast {ant.delete(dir: 'src/main/resources/static/built')}}
```

- git add .

- git commit -m "delete built functionality added to clean task"

- git push origin tut-basic-ant

- git checkout master

- git merge tut-basic-ant

- git tag -a ca2-part2 -m "end of assignment 2 part 2"

- git push origin ca2-part2
