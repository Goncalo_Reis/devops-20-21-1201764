package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void getFirstName_success() {
        Employee employee = new Employee("Frodo", "Baggins", "small",
                "ring bearer", "frodo@gmail.com");
        String expected = "Frodo";

        String result = employee.getFirstName();

        assertEquals(expected, result);
    }

    @Test
    void getFirstName_emptyValue() {
        Employee employee = new Employee("", "Baggins", "small",
                "ring bearer", "frodo@gmail.com");
        String expected = "Frodo";

        String result = employee.getFirstName();

        assertNotEquals(expected, result);
    }



    @Test
    void getLastName() {
        Employee employee = new Employee("Frodo", "Baggins", "small",
                "ring bearer", "frodo@gmail.com");
        String expected = "Baggins";

        String result = employee.getLastName();

        assertEquals(expected, result);
    }

    @Test
    void getLastName_emptyValue() {
        Employee employee = new Employee("Frodo", "", "small",
                "ring bearer", "frodo@gmail.com");
        String expected = "Baggins";

        String result = employee.getLastName();

        assertNotEquals(expected, result);
    }

    @Test
    void getDescription() {
        Employee employee = new Employee("Frodo", "Baggins", "small",
                "ring bearer", "frodo@gmail.com");
        String expected = "small";

        String result = employee.getDescription();

        assertEquals(expected, result);
    }

    @Test
    void getDescription_emptyValue() {
        Employee employee = new Employee("Frodo", "Baggins", "",
                "ring bearer", "frodo@gmail.com");
        String expected = "small";

        String result = employee.getDescription();

        assertNotEquals(expected, result);
    }

    @Test
    void getJobTitle() {
        Employee employee = new Employee("Frodo", "Baggins", "small",
                "ring bearer", "frodo@gmail.com");
        String expected = "ring bearer";

        String result = employee.getJobTitle();

        assertEquals(expected, result);
    }

    @Test
    void getJobTitle_emptyValue() {
        Employee employee = new Employee("Frodo", "Baggins", "small",
                "", "frodo@gmail.com");
        String expected = "ring bearer";

        String result = employee.getJobTitle();

        assertNotEquals(expected, result);
    }

    @Test
    void getEmail() {
        Employee employee = new Employee("Frodo", "Baggins", "small",
                "ring bearer", "frodo@gmail.com");
        String expected = "frodo@gmail.com";

        String result = employee.getEmail();

        assertEquals(expected, result);
    }

    @Test
    void getEmail_emptyValue() {
        Employee employee = new Employee("Frodo", "Baggins", "small",
                "ring bearer", "");
        String expected = "frodo@gmail.com";

        String result = employee.getEmail();

        assertNotEquals(expected, result);
    }

    @Test
    void getEmail_incorrectValue() {
        Employee employee = new Employee("Frodo", "Baggins", "small",
                "ring bearer", "frodogmail.com");
        String expected = "";

        String result = employee.getEmail();

        assertNotEquals(expected, result);
    }

    @Test
    void testToString_success() {
        Employee employee = new Employee("Frodo", "Baggins", "small",
                "ring bearer", "frodo@gmail.com");
        String expected = "Employee{" +
                "id=" + null +
                ", firstName='" + "Frodo" + '\'' +
                ", lastName='" + "Baggins" + '\'' +
                ", description='" + "small" + '\'' +
                ", jobTitle='" + "ring bearer" + '\'' +
                ", email='" + "frodo@gmail.com" + '\'' +
                '}';

        String result = "Employee{" +
                "id=" + employee.getId() +
                ", firstName='" + employee.getFirstName() + '\'' +
                ", lastName='" + employee.getLastName() + '\'' +
                ", description='" + employee.getDescription() + '\'' +
                ", jobTitle='" + employee.getJobTitle() + '\'' +
                ", email='" + employee.getEmail() + '\'' +
                '}';

        assertEquals(expected, result);
    }

    @Test
    void testToString_emptyValue() {
        Employee employee = new Employee("", "", "",
                "", "");
        String expected = "Employee{" +
                "id=" + null +
                ", firstName='" + "Frodo" + '\'' +
                ", lastName='" + "Baggins" + '\'' +
                ", description='" + "small" + '\'' +
                ", jobTitle='" + "ring bearer" + '\'' +
                ", email='" + "frodo@gmail.com" + '\'' +
                '}';

        String result = "Employee{" +
                "id=" + employee.getId() +
                ", firstName='" + employee.getFirstName() + '\'' +
                ", lastName='" + employee.getLastName() + '\'' +
                ", description='" + employee.getDescription() + '\'' +
                ", jobTitle='" + employee.getJobTitle() + '\'' +
                ", email='" + employee.getEmail() + '\'' +
                '}';

        assertNotEquals(expected, result);
    }
}