# Class Assignment 2 Report

- do ./gradlew build 

- do % java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

- in build.gradle:
```
task executeServer(type:JavaExec, dependsOn: classes) {
  group = "DevOps"
  description = "Launches a chat server on localhost:59001 "

  classpath = sourceSets.main.runtimeClasspath

  main = 'basic_demo.ChatServerApp'

  args '59001'
  }
```

- add test to src/test/java/basic_demo:
```
    @Test public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
```

- add the following code to build.gradle:
in dependencies:
```
    testImplementation 'org.junit.jupiter:junit-jupiter-api:5.7.1'
    testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine'
    testCompileOnly 'junit:junit:4.13'
    compile 'junit:junit:4.13'
    testRuntimeOnly 'org.junit.vintage:junit-vintage-engine'
```

and add this too:
```
sourceSets {
    main {
        java {
            srcDir 'src'
            srcDir 'test'
        }
    }
}
```

- to add a copy task that backups the src folder to a backup folder,
add this to build.gradle:
```
task backup(type: Copy) {
    from "src"
    into "backup"
}
```

- to add a zip file of the src contents, add this task:
```
  task zipBackup(type: Zip) {
    archiveFileName = "backup.zip"
    destinationDirectory = file("zipBackup")
    from "src"
}
  ```

- to add a tag marking the end of the assignment, input ```git tag -a ca2-part1 -m "end of assignment"```, 
and then ```git push origin ca2-part1````to share that to remote.