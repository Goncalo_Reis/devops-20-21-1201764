# Class Assignment 1 Report

## 1. Analysis, Design and Implementation

- ```git tag -a v1.2.0 -m "version 1.2.0"``` to tag the initial version as v1.2.0.

- ```git push origin v1.2.0``` to share that tag to remote.
  
- ```git branch email-field``` to create a new branch where I'll add support to an email-field.

- ```git checkout email-field``` to switch to that branch.

- add basic support for email field and the requested unit tests.

- set a breakpoint through Intellij where needed and then right-click on ReactAndSpringDataRestApplication class,
  and click on debug to do it on the server side.
  
- install the "react developer tools" extension on chrome, right-click in the page and select 'inspect'
  to open the developer tools. From there, open the 'Sources' tab and you can set breakpoints on the
  source code to debug it through the client side.

- ```git add``` to stage the changes in email-field branch.
  
-```git commit -m "support to email field"``` to commit those changes.

- ``` git push origin email-field``` to push them to remote.

-``` git checkout master``` to switch back to master.

- ```git merge email-field``` to get the master branch on par with email-field, now fully tested.

-´´´git push origin master´´´ to push that merge to remote.

- ```git tag -a v1.3.0 -m "version 1.3.0"``` to add a tag to the new version of the app,
  now with the email field included.

- ```git push origin v1.3.0``` to share that tag to remote.

- ```git branch fix-invalid-email``` to create a new branch to fix the email validation.

- ```git checkout fix-invalid-email``` to switch to that branch. 

- add support for email field validation and the requested unit tests.

- set a breakpoint through Intellij where needed and then right-click on ReactAndSpringDataRestApplication class,
  and click on debug to do it on the server side.

- right-click in the page and select 'inspect' to open the developer tools. From there, open the 'Sources'
  tab, and you can set breakpoints on the source code to debug it through the client side.

- ```git add .``` to stage the changes in email-field branch.

- ```git commit -m "validation fix to email"``` to commit those changes.

- ```git push origin fix-invalid-email``` to push them to remote.

-``` git checkout master``` to switch back to master.

- ```git merge fix-invalid-email``` to get the master branch on par with fix-invalid-email.

-´´´git push origin master´´´ to push that merge to remote.

- ```git tag -a v1.3.1 -m "version 1.3.1"``` to add a tag to the new version of the app,
  now with the email field validation fix included.

- ```git push origin v1.3.1``` to share that tag to remote.

- ```git tag -a ca1 -m "class assignment 1 done"``` to mark the repository.

- ```git push origin ca1``` to share that tag to remote.

## 2. Analysis of an Alternative

- Both Git and Mercurial are Distributed Version Control Systems (DVCS) created in 2005
  
- Git is currently considered the leading system in the market and Mercurial has been losing market share over the years.

- Both Git and Mercurial allow each developer in a project to have their own copy of the remote repository instead of
  relying on a single central server.
  
- There is no staging area before each commit in Mercurial by default. However, some Mercurial extensions can be installed
to perform this step.

-In Git, a branch is a reference to a certain commit. a branch can be easily created, altered and deleted without
affecting the version history.

- In Mercurial, a branch refers to a sequence of changesets. This means that a branch is embedded in the commits, 
  therefore they can't be removed because it would have to rewrite the version history.
  
- Mercurial might be better suited to less-experienced developers and smaller teams.
  
- Mercurial's commands and documentation appear to be easier to understand;

## 3. Implementation of the Alternative

- Link of the alternative repository: https://helixteamhub.cloud/shiny-sunset-7387/projects/devops/repositories/devops-20-21-1201764/tree/default

- ```hg clone``` and the repository url to initialize a local mercurial repository.

- ```hg add``` to add the files to be committed.

- ```hg commit -m "hgignore added"``` to commit the files to repository.

- ```hg push``` to push the commit to the remote repository.

- move the necessary project files into your local repository via explorer.

- ```hg add``` to add the files to be committed.

- ```hg commit -m "initial version"``` to commit the files to repository.

- ```hg tag v1.2.0 -m "version 1.2.0"``` to tag the initial version as v1.2.0 and commit it.

- ```hg push``` to share that tag to remote.

- ```hg branch email-field``` to create a new branch where I'll add support to an email-field.

- ```hg checkout email-field``` to switch to that branch.

- add basic support for email field and the requested unit tests.

- set a breakpoint through Intellij where needed and then right-click on ReactAndSpringDataRestApplication class,
  and click on debug to do it on the server side.

- install the "react developer tools" extension on chrome, right-click in the page and select 'inspect'
  to open the developer tools. From there, open the 'Sources' tab and you can set breakpoints on the
  source code to debug it through the client side.

- ```hg add``` to stage the changes in email-field branch.

-```hg commit -m "support to email field"``` to commit those changes.

- ``` hg push --new-branch``` to push them to remote to a new remote branch (email-field).

-``` hg checkout default``` to switch back to default.

- ```hg merge email-field``` to get the master branch on par with email-field, now fully tested.

- ```hg commit -m "merge email-field to default"``` to commit that change.

- ```hg push``` to push that merge to remote.

- ```hg tag v1.3.0 -m "version 1.3.0"``` to tag the new version as v1.3.0 and commit it.

- ```hg push``` to share that tag to remote.

- ```hg branch fix-invalid-email``` to create a new branch to fix the email validation.

- ```hg checkout fix-invalid-email``` to switch to that branch.

- add support for email field validation and the requested unit tests.

- set a breakpoint through Intellij where needed and then right-click on ReactAndSpringDataRestApplication class,
  and click on debug to do it on the server side.

- right-click in the page and select 'inspect' to open the developer tools. From there, open the 'Sources'
  tab, and you can set breakpoints on the source code to debug it through the client side.

- ```hg add``` to stage the changes in email-field branch.

- ```hg commit -m "validation fix to email"``` to commit those changes.

- ```hg push --new-branch``` to push them to remote.

-``` hg checkout default``` to switch back to master.

- ```hg merge fix-invalid-email``` to get the master branch on par with fix-invalid-email.

- ```hg push``` to push that merge to remote.

- ```hg tag v1.3.1 -m "version 1.3.1"``` to add a tag to the new version of the app and commit it,
  now with the email field validation fix included.

- ```hg push``` to share that tag to remote.

- ```hg tag ca1``` to mark the repository.

- ```hg push``` to share that tag to remote.


