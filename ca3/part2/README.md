#Class Assignment 3

##Part 2

The goal of Part 2 of this assignment is to use Vagrant to set up a virtual environment
to execute the tutorial spring boot application, gradle "basic" version.

- Go to  https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/ and study the VagrantFile available there.

- You should use that VagrantFile, but update the repository from which the tut-basic gradle is retrieved to your own.
To do that, change the following line in the VagrantFile as per below, at line 70:

![repository_change](../media/repository_change.png)

- It is important to input your password after the username in this step, or the VagrantFile might fail when run;

- After that, you should also get into the tut-basic-gradle project. That might be different for you depending 
on your folder structure, but here it suffices to do ````cd tut-basic-gradle````, as per line 71 of the picture above.
  You can leave the following lines unchanged.
  
- Before running the VagrantFile though, we need to set up our project properly.
    
    - Create a new class "ServletInitializer" and locate it within the folder structure as per the picture below:

![Servlet_Initializer](../media/Servlet_Initializer.png)

- Add ````id war```` at line 6, and the dependency at line 29 of the image below, in Gradle.build:

![Gradle_Build](../media/Gradle_Build.png)

- Set up the application.properties like it is represented in the picture below:

![Application_Properties](../media/Application_Properties.png)

At line 18, update the path to be equal to the one in the picture below:

![App_js](../media/App_js.png)

- Finally, with all these changes, we can run ````vagrant up```` on the folder where we located the VagrantFile,
and using the following URL's (or localhost), the results should be as follows:

![part2a_web](../media/part2a_web.png)

![part2a_db](../media/part2a_db.png)

- The string to connect to the database is: jdbc:h2:tcp://192.168.33.11:9092/./jpadb

## Alternative to VirtualBox - Hyper-V

###Analysis

Hyper-V and VirtualBox are applications designed to run virtual machines.
Hyper-V is a type 1 hypervisor that manages operating systems by running directly on a computer’s hardware.
On the other hand, VirtualBox is a type 2 hypervisor, which runs on the host operating system.

Moreover, Hyper-V is simple to use once it’s implemented, as the virtual machines run as long as the hardware is running,
but it can be difficult to set up, contrary to  VirtualBox setups, that are usually simple.

Another thing to keep in mind is that Hyper-V can’t handle non-Windows operating systems, while VirtualBox does so.

Lastly, VirtualBox can’t create virtual machines with the same performance as Hyper-V.

Regarding my own utilization of these two products, I can focus also on their integration with the Vagrant software,
and in that regard Virtualbox provided a much more simple and trouble-free way for me to set up what I needed than Hyper-V.
The main culprit of it was the fact that we cannot do the network setup through the VagrantFile with Hyper-V as we can with
VirtualBox. That limitation is described here: https://www.vagrantup.com/docs/providers/hyperv/limitations, and greatly reduces the usefulness
of Hyper-V for this use-case, making VirtualBox a far more preferable option.

###Implementation

To enable Hyper-v in your Windows machine, first you have to check if your version of Windows 10 is either
Pro, Enterprise, or Education. If it is, you can go ahead to control panel -> Programs ->Turn Windows features on or off.
  There, you'll see this window, where you must activate Hyper-v for it to be usable in your machine, as such:

![hyperv_activation](../media/hyperv_activation.png)

However, if your Windows version is not one of these three, you don't have access to this option. At least right away.
Following the steps explained here (https://www.itechtics.com/enable-hyper-v-windows-10-home/) should help gain access to Hyper-v
even on a Windows 10 Home version.

Before moving forward, there's another Windows feature we need to turn on. Again, going to control
panel -> Programs ->Turn Windows features on or off, we want to turn on the SMBv1,
a networking file share protocol that will be used to mediate communication between our guest and host machines,
as displayed below:

![SMB](../media/SMB.png)

Access https://app.vagrantup.com/generic/boxes/ubuntu1604/versions/3.2.18, and replace the lines present in the
following pictures.

![hyperv_box](../media/hyperv_box.png)

![vagrant_1](../media/vagrant_1.png)

![vagrant_2](../media/vagrant_2.png)

![vagrant_3](../media/vagrant_3.png)

We also need to do the following modifications to make sure that we are cloning our own repository to the
web VM and that we enter the project correctly:

![vagrant_4](../media/vagrant_4.png)

Before running the VagrantFile, we might have to do one more thing: depending on the Vagrant version
 you have installed on your computer, you might be plagued with an error that is affecting the SMB protocol
we want to use. It's an _ArgumentError_, and you can find out more about it here: https://github.com/hashicorp/vagrant/issues/12277.
To solve it, you must follow this path into your Vagrant install directory: 
Hashicorp/Vagrant/embedded/gems/2.2.15/gems/vagrant-2.2.15/plugins/guests/linux/cap/mount_smb_shared_folder.rb. At line 23,
where you see the arguments (:mount_name, options), you should change to (:mount_name, name, options) and that should solve it.
Again, this is an error identified by Hashicorp and will soon be solved by the team, but in the meantime there's this workaround.
You can see the file mentioned, and the necessary change in the below picture:

![SMB_Workaround](../media/SMB_Workaround.png)

Now, you can go to the directory where you have your VagrantFile and run ````vagrant up````. You must run your command line
 of choice as administrator, or you won't get access to Hyper-V.

When that is done, and because of the network limitations of the Vagrant-Hyper-v integration, you must do some extra steps
to set it up yourself:

- ````vagrant ssh web```` to enter the web VM you just created.
  
- Go inside the tut-basic-gradle project, into the src folder -> main -> resources, and do ````sudo nano application.properties```` to open that file.

- Open the Hyper-V Manager and find the IP address of the DB Virtual Machine.

- In the application properties, replace the existent IP Address in the spring.datasource.url with this Ip from the DB Virtual Machine you just fetched.

- Exit the ssh and do ````vagrant reload --provision```` to reload the project, now with the correct IP Address.

Finally, you can access the frontend and backend of the application using the IP address of the web VM, as follows:

![part2bWeb_success](../media/part2bWeb_success.png)

![part2bDb_success](../media/part2bDb_success.png)

- The string to connect to the database is: jdbc:h2:tcp://[_IP address DB Virtual Machine_]:9092/./jpadb


