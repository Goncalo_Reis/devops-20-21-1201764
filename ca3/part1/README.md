# Class Assignment 3 Report

##Part 1

The goal of the Part 1 of this assignment is to practice with VirtualBox using the same
projects from the previous assignments but now inside a VirtualBox VM with Ubuntu.

###Tut-Basic-Gradle

To get the Tut-Basic-Gradle to work through Ubuntu VM, the following steps are necessary:

- Create the vm as learned in the lecture (this was done in detail in the
  class, so it is not going to be expanded here);

- Clone your repository to the VM using the ````git clone```` command;

- Navigate to the tut-basic-gradle project
  (````cd devops-20-21-1201764```` -> ````cd ca3```` -> ````cd part1```` -> ````cd tut-basic-gradle````)

- Use the ````chmod u+x gradlew```` command to give the necessary permissions to run gradlew;

- Use ````./gradlew build```` to build the project inside the VM;

- Finally, use ````./gradlew bootRun```` to run the app. If successful, you'll be able to see
  the webpage in a URL containing the IP address of the VM at port 8080, like this:

![part1a-success](../media/part1a-success.png)

###Gradle-Basic-Demo - Chat Application

To get the chat application to work through Ubuntu VM, the following steps are necessary:

- Clone your repository to the VM using the ````git clone```` command;

- Navigate to the Gradle-Basic-Demo project
  (````cd devops-20-21-1201764```` -> ````cd ca2```` -> ````cd part1````);

- Use the ````chmod u+x gradlew```` command to give the necessary permissions to run gradlew;

- Use ````./gradlew build```` to build the project inside the VM;

- Use java ````-cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp```` to get the chat server running;

- Then, one can open two chat windows in the host using ````./gradlew runClient````, as per the picture below:

![part1a-success](../media/part1b-success.png)

- Finally, tag the last commit with "ca3-part1" using the ````git tag -a ca3-part1 -m "class assignment 3 part 1"```` command,
  and do ````git push origin ca3-part1```` to push the tag to remote.
