# Class Assignment 5 Part 1 Report

## 1.Implementation

In this first section, we'll learn how to set up a Jenkins pipeline. We'll use the earlier-created project 'gradle basic demo part 1' for this.

### 1.1 Installing Jenkins

Jenkins was previously downloaded as a war file and run with the command: **java -jar jenkins.war**.

Then we navigate to localhost:8080, create a login, and we're ready to start building our first pipeline.

### 1.2 Jenkinsfile

I began by creating the ca5 and part1 folders and cloning the ca2/part1 project into them.
Then I created the Jenkinsfile and used the command './gradlew build' to build the project, which resulted in the creation of a folder called build.

![jenkinsfile](../media/jenkinsfile_part1.png)

The most difficult aspect of this project is creating this file. Although it is straightforward, it may be essential to make some changes depending on how it is constructed, as was the case in my instance.

First, I created the pipeline structure as requested, which included the Checkout, Assembling, Testing, and Archiving sections.

Because I'm using Windows instead of Unix, I'll need to utilize the bat command rather than the ssh command. Also, one must use the command 'call [path of the file gradlew.bat] -p [name of the file where you want to execute] [relevant gradlew command]' to launch gradlew.

We can see how the Jenkinsfile is set up in the image above.

For the tests, I used the Junit plugin available for Jenkins. if you set up your jenkins account with the recommended plugins, the jenkins will already have that plugin installed.

Then, as shown in the jenkinsfile image above, I only needed to archive using the archiveArtifacts command.

### 1.3 Creating the pipeline

I proceeded to my Jenkins page and created a new job after organizing the Jenkinsfile.

I gave it the name 'ca5 Part1' and chose the Pipeline option.

Then I chose the option Pipeline script from SCM for jenkins to be able to read the Jenkinsfile from my own repository.

Additionally, I downloaded the Open Blue Ocean plugin to assist me follow the process and see exactly what issues were occurring.

![pipeline](../media/pipeline_success.png)

I ran the pipeline and as the above figure shows, everything went as expected.