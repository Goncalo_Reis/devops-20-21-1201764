# Class Assignment 5 Part 2 Report

## 1. Implementation

### 1.1 Building

I started by cloning the ca2/part2 project to ca5/part2 and building it.

I added a Jenkinsfile and a Dockerfile once the build was completed.

### 1.2. Jenkinsfile

The Pipeline final configuration can be seen here, with necessary explanations below.

![jenkinsfile_part2](../media/jenkinsfile_part2.png)

I started by downloading the Javadoc Plugin and restarting Jenkins to make the Javadoc portion.
Then I looked up how to publish to Jenkins as requested, and went to the Pipeline Syntax (in my case I opened another pipeline that I already had to find this menu).
As you can see in the screenshot below, I selected PublishHTML as a Sample Step, then entered the location to the javadoc file and generated the Pipeline Script, which generated the syntax, which I inserted into the Jenkinsfile.

![pipeline_syntax](../media/pipeline_syntax.png)

As you can see in the screenshot below, I selected PublishHTML as a Sample Step, then entered the location to the javadoc file and generated the Pipeline Script, which generated the syntax, which I inserted into the Jenkinsfile.
As can be seen on the Stage ('Javadoc'), I first ask gradlew to execute the javadoc, which generates the docs file inside the build file.
And the line below is in charge of publishing to Jenkins; when you've built the pipeline, it should look like this.

![javadoc](../media/javadoc.png)
--
![javadoc_page](../media/javadoc_page.png)

After that, I needed to add the Dockerfile to the project so that I could create a Tomcat image.
Installing git, node, and npm were the first steps. After that, I add the.war file to Tomcat's path.

Docker Pipeline is the plugin required to construct and use Docker from pipelines, therefore I downloaded it.
To build and publish the image, I first had to generate a Jenkins credential to access my Docker Hub repository.
Then, in the first line, I produced the image and then published it using the docker hub credential I created previously, as per the syntax I researched.
I basically told Jenkins to produce an image called "app" based on the *docker.build* line that builds the image.
The *docker.build* accepts the repository in the format "username/repository," which is in the Dockerhub, and I then indicated the path to the Dockerfile because it is in the pipeline workspace at this point.

![docker](../media/docker.png)

After that, all I had to do was create a new pipeline job and point it to the Jenkinsfile in my repository, then build.
When I clicked on the status after completing the build, I saw the image below.

![jenkins_success](../media/jenkins_success_part2.png)

As we can see above, the objectives of part2 are, then, concluded.

## 2. Alternative Analysis

As an alternative to Jenkins, I decided to look at Buddy. Buddy is a Polish company that was established in 2015. Buddy is a web-based continuous integration and delivery (CI/CD) pipeline solution for building, testing, and deploying websites and applications using code from GitHub, Bitbucket, and GitLab.

It comes in two flavors: public cloud and on-premises. Docker layer caching, pipelines and concurrent stages, parallel build and testing, repository and artifact caching, and vCPU and RAM scalability are among the cloud and on-premises options.

It can be used with Blockchain technology in addition to Docker and Kubernetes integration. Buddy runs all of its builds and commands in separate Docker containers. Buddy also integrates with AWS, Digital Ocean, Kubernetes, Microsoft Azure, Rackspace, and other notable cloud providers.

Buddy makes it simple to set up a pipeline because the configuration is done through the GUI. There is an option to export the pipeline settings to a YAML file right away (to use the pipeline as code).

One disadvantage is that you must pay for a more comprehensive version, which starts at $75 per month each team or $35 per month per user.

On top of that, there's also some more key changes between the two:

- Buddy has Debugging capabilities; Jenkins has not.  
- Buddy has Quality Assurance capabilities; Jenkins has not.  
- Both have test management capabilities.
- Only Jenkins has deployment in Linus.
- Buddy has a build log, while jenkins does not.
- Buddy has configuration management capabilities, while Jenkins lacks such feature.
- Both have Continuous Delivery and Continuous Deployment capabilities.
- Only Buddy has capabilities of deployment on Android.
- Buddy does not need to be installed, while jenkins does.
- Buddy is only partially free, while Jenkins is completely free.
- Both have access control/permissions.

As can be seen from the features comparison table above, Buddy is the product that offers the greatest benefits.
Of course, Jenkins will be the greatest option depending on the sort of organization and task, but Buddy is better overall.

## 3. Alternative Implementation

The following steps were required to create the Buddy pipeline:

- Entering Buddy's website: https://buddy.works/;

- Creating an account and linking the bitbucket account;

- Syncing the devops repository;

After that, we needed to add a pipeline, and then do the following:

- Select Actions and, then, the Gradle container. In Run, it was necessary to add the following code:
    - cd ca5/part2/alternative
    - gradle assemble
    - gradle test
    - gradle javadoc
- In Environment, it was important to select the 6.8.3 version of gradle so we ran into no issues, and in Actions give an appropriate name for this step, if wanted.

- Add another action, the Build Docker image. After clicking it, in setup, it was needed to enter the path to the Dockerfile ('ca5/part2/alternative/Dockerfile', in my case),
  and in Options I link to the Docker hub adding the necessary information like username and password there.

![docker alternative](../media/docker_alternative.png)

Everything ran as expected. Buddy does not have such a good integration with Javadoc such as Jenkins has, but we can go to filesystems in Buddy and see the javadoc, in build/docs.

Lastly, it was time to see if the image was really published to docker hub. 

![build alternative](../media/build_alternative.png)

As we can see, it was successful, and so this task has been completed.
